import csv

slozeni = []
nazvy = {}
dodavky = {}
vydej = {}

with open('pripravky.csv', newline='') as nazvy_csv:
	data = csv.reader(nazvy_csv, delimiter=';')
	for row in data:
		nazev = row[38]
		nazvy[row[0]] = nazev #mame slovnik id=nazev
		dodavky[row[0]] = row[33] #mame slovnik id=stav dodavky	
		vydej[row[0]] = row[29] #mame slovnik id = moznost vydeje

#vycuc listu leku & ucinnych latek

with open('slozeni.csv', newline='') as slozeni_csv:
	data = csv.reader(slozeni_csv, delimiter=';')
	for row in data:
		if row[3] == "" and dodavky[row[0]] == "1" and vydej[row[0]] == "F": #rovnou filtrujeme pomocne latky, nedodavane a volne neprodejne leky
					slozeni.append(row)

#parsing na radky: id, (:id latky, mnozstvi latky, jednotka mnozstvi:)

leky = []
previndex = 0
lek = []

for row in slozeni:
	index = row[0]
	if index != previndex:
		if lek: leky.append(lek)
		lek = [row[0],row[1],row[5],row[6]]
	else: 
		lek.extend([row[1],row[5],row[6]])
	previndex = index
leky.append(lek)

#serazeni dle identickeho slozeni

duplikaty = []

for lek1 in leky:
	zaznam = [lek1[0]]
	for lek2 in leky:
		if lek1[0] != lek2[0] and lek1[1:] == lek2 [1:]:
			zaznam.append(lek2[0])
	zaznam.sort()
	if zaznam not in duplikaty and len(zaznam)>1: 
		duplikaty.append(zaznam)

#prirazeni jmen

duplikaty_names = []

for rada in duplikaty:
	dup_name = []
	for polozka in rada:
		dup_name.append(nazvy[polozka])
	dup_uniq = list(set(dup_name)) #pryc jmenne shody (stejny lek, jine mnozstvi)
	if len(dup_uniq)>1:
		duplikaty_names.append(dup_uniq)

for duplikat in duplikaty_names: print(duplikat)