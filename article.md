---
title: "Stejný účinek, ale vyšší cena. Velké srovnání léků, na kterých lze ušetřit"
perex: "Kupujete léky bez předpisu jen podle reklamy nebo doporučení? Může se vám to prodražit. Na trhu často najdete přípravky se stejným obsahem totožné účinné látky, ale s výrazně nižší cenou. Podle odborníků ale nemusí být o nic méně kvalitní."
description: "Kupujete léky bez předpisu jen podle reklamy nebo doporučení? Může se vám to prodražit. Na trhu často najdete přípravky se stejným obsahem totožné účinné látky, ale s výrazně nižší cenou. Podle odborníků ale nemusí být o nic méně kvalitní."
authors: ["Kristina Lamperová", "Michal Zlatkovský"]
published: "7. dubna 2017"
socialimg: https://interaktivni.rozhlas.cz/leky-generika/media/social.jpg
url: "leky-generika"
# libraries: [d3, topojson, jquery, highcharts, leaflet, inline-audio]
libraries: [jquery, "https://interaktivni.rozhlas.cz/tools/datatables/1.10.12.min.js"]
styles: ["https://interaktivni.rozhlas.cz/tools/datatables/1.10.12.min.css"]
recommended:
  - link: http://www.rozhlas.cz/zpravy/data/_zprava/leky-v-datech-na-cem-jsme-zavisli-kolik-utracime--1465581
    title: Léky v datech: Na čem jsme závislí, kolik utrácíme?
    perex: Léků bereme rok od roku víc. Nejde ovšem jen o mediálně propíraná antibiotika nebo vakcíny. Rychleji dnes roste spotřeba antidepresiv, prášků na srdce nebo léků na rakovinu. Jaké léky bereme? Kdo určuje ceny? Kolik zaplatíte vy, kolik pojišťovna a na kolik si přijdou farmaceutické firmy? Jak změnily situaci regulační poplatky?
    image: https://samizdat.cz/data/spotreba-leku-snowfall/www/img/4.jpg
  - link: https://interaktivni.rozhlas.cz/umrti-srdce/
    title: Na co se kde v Evropě nejčastěji umírá? Podívejte se na podrobnou mapu
    perex: Češi mají díky lékařské péči největší šanci na přežití srdečního infarktu na světě. Ani to ale nestačí: počtem úmrtí na nemoci srdce patříme do méně civilizované části Evropy. Umíráme na ně třikrát častěji než Francouzi.
    image: https://interaktivni.rozhlas.cz/umrti-srdce/media/socimg.png
---
<style>tr.group,
tr.group:hover {
    background-color: #ddd !important;
}</style>

<div data-bso="1"></div>

Rozdíl v cenách volně prodejných léků, které mají stejný podíl účinné látky a podobnou velikost balení, může být i velmi výrazný: sedm tablet léku proti alergiím Zyrtec vyjde na 112 korun, ve stejném obchodě pořídíte 10 tablet Alerid za necelých 50 korun. Obsah účinné látky mají tablety v obou baleních stejný - 10 miligramů. Ze srovnání, které Český rozhlas provedl na základě dat Státního ústavu pro kontrolu léčiv, vyplývá, že takových skupin léků je celá řada.  

<aside class="big">
<table id="example" class="display responsive nowrap"></table>
<i>Zdroj dat: [SÚKL](https://opendata.sukl.cz/), zdroj cen léků: [lekarna.cz](https://www.lekarna.cz/). Aktuální k 31. 3. 2017.</i>
</aside>

Podle přednosty Farmakologického ústavu 1. LF UK Ondřeje Slanaře přitom odlišná cena většinou neznamená odlišnou kvalitu. "Obecně řečeno může být u konkrétních léků úprava lékové formy - třeba když se léčivá látka uvolňuje postupně, bývá lék dražší, než když se uvolní naráz. V drtivé většině případů ale bývají při stejném množství účinné látky účinky srovnatelné."

Ceny léků, které si v lékárně můžete koupit bez receptu, nepodléhají státní regulaci. To v praxi znamená, že výrobci nejsou nijak omezeni v tom, za kolik peněz je můžou prodávat. Mast na bolest kloubů může stát 20 i 150 korun.

"Cena je dána tím, kdo léky vyrábí. Pokud je to renomovaná společnost s výrobou v EU nebo USA, výrobní náklady jsou vyšší a promítnou se i náklady na marketing, logistiku a distribuci," popisuje výkonný ředitel Asociace inovativního farmaceutického průmyslu Jakub Dvořáček. "U volně prodejných léků je cena dána také tím, jaké marže si účtují jednotlivé lékárny. Maximální ceny ani úhrady nejsou stanoveny."

Pacienti v lékárnách přitom většinou netuší, že mají na výběr mezi nákupem takzvaných originálních léků a generik. V prvním případě jde o léky, jejichž výrobce dané účinné látky přímo vynalezl a investoval do jejich výzkumu. Generické léky jsou oproti tomu kopií originálu a snaží se jim konkurovat především cenou. Mohou se lišit ve složení pomocných látek a v technologii zpracování, ale účinná látka generik a originálních léků je identická.

Například v případě populárního přípravku proti bolesti s účinnou látkou ibuprofenem byl originálním lékem Brufen, který vznikl ve Velké Británii a v Československu se prodával už v roce 1973. Dnes je ibuprofen v Česku dostupný i prostřednictvím celé řady generických léků jako Ibalgin a Nurofen. Cena nejlevnějšího z nich je přitom oproti Brufenu poloviční.