  var data = [
["Zánět a bolest: ibuprofenum 400 mg","Ibuprofen AL","30 tbl","32 Kč"],
["Zánět a bolest: ibuprofenum 400 mg","Ibalgin","24 tbl","45 Kč"],
["Zánět a bolest: ibuprofenum 400 mg","Apo-ibuprofen","30 tbl","46 Kč"],
["Zánět a bolest: ibuprofenum 400 mg","Ibumax","30 tbl","49 Kč"],
["Zánět a bolest: ibuprofenum 400 mg","Brufen","30 tbl","64 Kč"],  
["Zánět a bolest: ibuprofenum 400 mg","Nurofen","24 tbl","69 Kč"],

["Zánět a bolest: ibuprofenum 5 % krém","Dolgit krém","100 gm","99 Kč"],
["Zánět a bolest: ibuprofenum 5 % krém","Ibalgin krém","100 gm","136 Kč"],

["Zánět a bolest: diclofenacum natricum 140 mg léčivá náplast","Olfen","5 npl","199 Kč"],
["Zánět a bolest: diclofenacum natricum 140 mg léčivá náplast","Voltaren","5 npl","302 Kč"],

["Bolest: paracetamol 500 mg","Paralen","24 tbl","32 Kč"],
["Bolest: paracetamol 500 mg","Panadol Novum","24 tbl","34 Kč"],
["Bolest: paracetamol 500 mg","Paramax Rapid","30 tbl","36 Kč"],

["Alergie: cetirizini dihydrochloridum 10 mg","Alerid","10 tbl","46 Kč"],
["Alergie: cetirizini dihydrochloridum 10 mg","Cetixin","10 tbl","56 Kč"],
["Alergie: cetirizini dihydrochloridum 10 mg","Analergin","10 tbl","69 Kč"],
["Alergie: cetirizini dihydrochloridum 10 mg","Zodac","10 tbl","72 Kč"],
["Alergie: cetirizini dihydrochloridum 10 mg","Apo-cetirizin","10 tbl","76 Kč"],
["Alergie: cetirizini dihydrochloridum 10 mg","Zyrtec","7 tbl","112 Kč"],

["Alergie: levocetirizinum 5 mg","Volnostin","14 tbl","102 Kč"],
["Alergie: levocetirizinum 5 mg","Levoxal","14 tbl","122 Kč"],
["Alergie: levocetirizinum 5 mg","Xyzal","14 tbl","126 Kč"],
["Alergie: levocetirizinum 5 mg","Analergin Neo","20 tbl","129 Kč"],

["Odkašlávání: acetylcysteinum 600 mg","NAC AL","20 tbl","112 Kč"],
["Odkašlávání: acetylcysteinum 600 mg","Mucobene","20 sáčků","134 Kč"],
["Odkašlávání: acetylcysteinum 600 mg","ACC Long","20 tbl","159 Kč"],
["Odkašlávání: acetylcysteinum 600 mg","Fluimucil","20 tbl","186 Kč"],

["Odkašlávání: ambroxoli hydrochloridum 30 mg","Ambroxol AL","20 tbl","39 Kč"],
["Odkašlávání: ambroxoli hydrochloridum 30 mg","Flavamed","20 tbl","44 Kč"],
["Odkašlávání: ambroxoli hydrochloridum 30 mg","Ambrobene","20 tbl","56 Kč"],
["Odkašlávání: ambroxoli hydrochloridum 30 mg","Ambrosan","20 tbl","59 Kč"],
["Odkašlávání: ambroxoli hydrochloridum 30 mg","Mucosolvan","20 tbl","76 Kč"],

["Odkašlávání: ambroxoli hydrochloridum 3 mg/ml sirup","Solvolan","100 ml","84 Kč"],
["Odkašlávání: ambroxoli hydrochloridum 3 mg/ml sirup","Mucosolvan","100 ml","116 Kč"],

["Průjem: loperamidi hydrochloridum 2 mg","Lopacut","10 tbl","89 Kč"],
["Průjem: loperamidi hydrochloridum 2 mg","Loperon","20 tbl","139 Kč"],
["Průjem: loperamidi hydrochloridum 2 mg","Imodium","20 tbl","174 Kč"],

["Pálení žáhy: hydrotalcitum 500 mg","Rutacid","20 tbl","74 Kč"],
["Pálení žáhy: hydrotalcitum 500 mg","Talcid","20 tbl","76 Kč"],

["Povzbuzení duševních funkcí: piracetamum 800 mg","Geratam","60 tbl","114 Kč"],
["Povzbuzení duševních funkcí: piracetamum 800 mg","Piracetam","100 tbl","149 Kč"],
["Povzbuzení duševních funkcí: piracetamum 800 mg","Pirabene","100 tbl","192 Kč"],

["Povzbuzení duševních funkcí: ginkgo bilobae extractum siccum normatum 40 mg","Gingio","90 tbl","244 Kč"],
["Povzbuzení duševních funkcí: ginkgo bilobae extractum siccum normatum 40 mg","Tebokan","100 tbl","296 Kč"],
["Povzbuzení duševních funkcí: ginkgo bilobae extractum siccum normatum 40 mg","Tanakan","90 tbl","289 Kč"],

["Plísňové infekce: clotrimazolum 1 % krém","Clotrimazol","20 gm","52 Kč"],
["Plísňové infekce: clotrimazolum 1 % krém","Canesten","20 gm","129 Kč"],

["Záněty pochvy: clotrimazolum 200 mg","Clotrimazol","3 tbl","59 Kč"],
["Záněty pochvy: clotrimazolum 200 mg","Candibene","3 tbl","76 Kč"],

["Zácpa: natrii picosulfas monohydricum 75 mg roztok","Regulax","10 ml","42 Kč"],
["Zácpa: natrii picosulfas monohydricum 75 mg roztok","Laxygal","10 ml","59 Kč"],
["Zácpa: natrii picosulfas monohydricum 75 mg roztok","Guttalax","15 ml","89 Kč"],

["Antidota: omeprazolum 20 mg","Omeprazol","14 tobolek","76 Kč"],
["Antidota: omeprazolum 20 mg","Loseprazol","14 tobolek","112 Kč"],
["Antidota: omeprazolum 20 mg","Apo-ome","14 tobolek","139 Kč"],

["Alopecie: minoxidilum 5% roztok","Belohair","60 ml","512 Kč"],
["Alopecie: minoxidilum 5% roztok","Regaine","60 ml","1 212 Kč"]
 ];

$(document).ready(function() {
    $('#example').DataTable( {
      responsive: true,
      ordering: false,
      paging:   false,
      scrollY:  '75vh',
      scrollCollapse: true,
      language: {
                url: "https://interaktivni.rozhlas.cz/tools/datatables/Czech.json" },
        order : [[ 2, "desc"]],
        data: data,
        columns: [
            { title: "Kategorie"},
            { title: "Název" },
            { title: "Balení" },
            { title: "Cena" },
        ],

        columnDefs: [
            {   
                targets : 0,
                visible: false,
                responsivePriority: 2  
            },
            {
                targets: 1,
            },
            {
                className: "dt-body-center",
                targets: 2,
                searchable: false
            },
            {   
                className: "dt-body-center",
                targets: 3,
                responsivePriority: 1,
                searchable: false
            },        
        ],
        "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
 
            api.column(0, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group"><td colspan="5"><b>'+group+'</b></td></tr>'
                    );
 
                    last = group;
                }
            } );
        }        
    });    
});